v0.1.1, April 11 2018 -- Minor amendment.
v0.1.0, September 15 2014 -- Removed 'fieldpy' dependency.
v0.0.2, September 12 2014 -- Initial release.
v0.0.1, September 08 2014 -- Initial release (undergoing testing).
