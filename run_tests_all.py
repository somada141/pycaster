# First, change to the HOME directory. This way we won't import the 'loose'
# version of pycaster which resides in the .git repo
import os
home = os.path.expanduser("~")
os.chdir(home)

# Then import the pycaster version that should have been installed in the
# Anaconda python and run the pycaster tests
from pycaster.test import test_all
test_all.runTests()
